package com.fundamentals;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
//@PropertySources({ Just in case we have more than one properties file
	@PropertySource("classpath:messages.properties") 
//})
public class MessagesPropertiesConfig {

}
