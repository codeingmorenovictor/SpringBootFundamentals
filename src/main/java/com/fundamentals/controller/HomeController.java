package com.fundamentals.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

	// Redirect method will automatically redirect to: /app/home path. URL changes
	@GetMapping("/redirect")
	public String goHome() {
		return "redirect:/app/home";
	}

	// Forward method won�t change the URL path, it�s generated in the background by a Dispatcher servlet
	// Forward CAN'T redirect to external WebSites
	@GetMapping("/forward")
	public String goHomeForward() {
		return "forward:/app/home";
	}

	@GetMapping("/google")
	public String redirectToExternalWebSite() {
		return "redirect:https://www.google.com";
	}

}
