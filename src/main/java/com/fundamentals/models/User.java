package com.fundamentals.models;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
//@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2870705498723911386L;

//	@EqualsAndHashCode.Include
//	private Integer key;
	private String name;
	private String pass;
	private String email;

	public User(String name, String pass, String email) {
		this.name = name;
		this.pass = pass;
		this.email = email;
	}

}
