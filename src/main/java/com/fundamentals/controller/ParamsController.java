package com.fundamentals.controller;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/params")
public class ParamsController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1092672187885753604L;

	@Value("${global.appTitle}")
	private String globalAppTitle;

//The syntax to send data through the URL would be:  params/string?text=Here goes the text
	@GetMapping(value = "/string")
	public String stringParamByUrl(
			@RequestParam(defaultValue = "This is a just a default value for @RequestParam!!!", required = false) String text,
			Model model) {
		model.addAttribute("title", globalAppTitle);
		model.addAttribute("header", "Value given by URL is :".concat(text));
		return "params";
	}

	@GetMapping("/integer")
	public String integerParamByURL(@RequestParam Integer number, Model model) {
		model.addAttribute("title", globalAppTitle);
		model.addAttribute("header", "Integer value given by url is: ".concat(String.valueOf(number)));
		return "params";
	}

	// Full syntax for URL would be: params/httpServlet?number=7&text=Some text goes
	// here
	@GetMapping("/httpServlet")
	public String paramsWithHTTPServletRequest(HttpServletRequest request, Model model) {
		Integer requestValue = null;
		try {
			requestValue = Integer.valueOf(request.getParameter("number"));

		} catch (NumberFormatException e) {
			requestValue = 0;
		}
		String text = request.getParameter("text");
		model.addAttribute("title", globalAppTitle);
		model.addAttribute("header", "The number given by URL using HttpServletRequest is: "
				.concat(String.valueOf(requestValue)).concat(" and the text is :".concat(text)));
		return "params";
	}

	@GetMapping("/pathVariable/{key}")
	public String paramsUsingPathVariable(@PathVariable Integer key, Model model) {
		model.addAttribute("title", globalAppTitle);
		model.addAttribute("header", "The number given by URL using @PathVariable is: ".concat(String.valueOf(key)));
		return "params";
	}
}
