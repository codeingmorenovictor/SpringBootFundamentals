package com.fundamentals.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fundamentals.models.User;

@Controller
@RequestMapping("/app")
public class IndexController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7210898883551662208L;

	private List<User> usersList = new ArrayList<User>();


	@Value("${indexController.sampleValue}")
	private String sampleValueFromProperties;
	
	//this value comes from Message.properties file
	@Value("${global.appTitle}")
	private String globalAppTitle;

	// Passing data to the view using ModelAndView
//	@GetMapping(value = { "/index","/"})
//	public ModelAndView goIndex(ModelAndView mv) {
//		mv.addObject("title", "Boot Fundamentals");
//		mv.addObject("header", "Spring Boot Fundamentals Data with ModelAndView");
//		mv.setViewName("index"); // name of the HTML view to be rendered
//		return mv;
//	}

	// @RequestMapping(value OR path, both can be used. value is an alias for Path
	@GetMapping(value = { "/home", "/", "", "/index" })
	public String goHome(Model model) {
		model.addAttribute("title", globalAppTitle); // Passing data to the view
		model.addAttribute("header", "Spring Boot Fundamentals");
		model.addAttribute("sampleValueFromProperties", sampleValueFromProperties);
		return "index";
	}

	@GetMapping(value = "/users")
	public String usersList(Model model) {
		retrieveAllUsers();
		model.addAttribute("title", globalAppTitle);
		model.addAttribute("header", "Users List");
		model.addAttribute("users", usersList);
		return "users";
	}

	public List<User> retrieveAllUsers() {
		usersList = Arrays.asList(new User("Jhon Doe", "1$%655", "jhnonDoe@gmail.com"),
				new User("Joaquin Phoenix", "phoenix12484", "jphoenix@gmail.com"),
				new User("Adam Lozano", "baer125", "adamx@gmail.com"),
				new User("Ernesth Moreno", "mosh1245", "moshe@gmail.com"),
				new User("Maria Maldonado", "goldis478", "mariaramirez@gmail.com"));
		return usersList;
	}

	// @ModelAttribute elements can be used in the entire project as a global Object
	@ModelAttribute("usersGeneric")
	public List<User> modelAtribute() {
		List<User> usersListInAllProject = new ArrayList<User>();
		usersListInAllProject = Arrays.asList(new User("Victor Moreno", "vic1202we", "vicmoreno@gmail.com"),
				new User("Joaquin Phoenix", "phoenix12484", "jphoenix@gmail.com"),
				new User("Jose Hernandez", "hernjose4512", "hernjose@gmail.com"),
				new User("Virginia", "120478", "virginia@gmail.com"),
				new User("Roberto Estrada", "robert1452", "robert_120@gmail.com"));
		return usersListInAllProject;
	}
}
